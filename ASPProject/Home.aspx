﻿<%@ Page Title="" Language="VB" MasterPageFile="~/PropertyMaintMaster.master" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="Home" Theme="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MasterContentPlaceHolder1" Runat="Server">
    <div class="container">
        <div class="starter-template">
            <div class="container">
                <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="panel-title">Sign In</div>
                        </div>
                        <div style="padding-top:30px" class="panel-body">
                            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                            <form id="loginform" runat="server" class="form-horizontal">
                                <input type="hidden" name="redirect" value="" />
                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <asp:TextBox ID="username" class="form-control" name="username" value="" placeholder="username" maxlength="50" required="true" runat="server"></asp:TextBox>
                                </div>
                                <div style="margin-bottom: 25px" class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <asp:TextBox ID="password" type="password" class="form-control" name="password" placeholder="password" maxlength="50" required="true" runat="server"></asp:TextBox>
                                </div>
                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->
                                    <div class="col-sm-12 controls">
                                        <asp:Button ID="Button1" runat="server" name="login" class="btn btn-success" Text="Login" /><i class="icon-hand-right"></i>
                                        <span class="help-inline"><strong><asp:Label ID="Label1" runat="server" Text=""></asp:Label></strong></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                            Don't have an account!
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="./Register.aspx">Sign Up Here</asp:HyperLink>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
