﻿Option Strict On
Imports System.Data
Imports System.Data.SqlClient
Partial Class Dashboard
    Inherits System.Web.UI.Page
    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GridView1.SelectedIndexChanged
        Dim row As GridViewRow = GridView1.SelectedRow
        Dim serviceRequestID As String = row.Cells(1).Text
        Response.Redirect("./ServiceRequestDetails.aspx?ServiceRequestID=" & serviceRequestID)
    End Sub

    Private Sub btnSearch_ServerClick(sender As Object, e As EventArgs) Handles btnSearch.ServerClick
        Dim searchTerm As String = txtSearchTerm.Text
        Dim searchType As String = selSearchType.Value
        Dim queryString As String = String.Empty
        Dim RoleID As Integer
        Dim UserID As Integer
        Dim UserInfo As Hashtable = CType(Session("UserInfo"), Hashtable)
        With UserInfo
            RoleID = CType(.Item("RoleID"), Integer)
            UserID = CType(.Item("UserID"), Integer)
        End With

        Select Case searchType
            Case "all"
                Select Case RoleID
                    Case 1 'Tenant
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE TenantID = {0}", UserID)
                    Case 2 'Landlord
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE LandlordID = {0}", UserID)
                    Case 3 'Service Staff
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE ServiceStaffID = {0}", UserID)
                End Select
                GlobalVariables.ds = GetData(queryString)
                If (GlobalVariables.ds.Tables.Count > 0) Then
                    GridView1.DataSource = GlobalVariables.ds
                    GridView1.DataBind()
                End If
            Case "status"
                Select Case RoleID
                    Case 1 'Tenant
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE TenantID = {0} AND WorkflowStatus LIKE '%{1}%'", UserID, searchTerm)
                    Case 2 'Landlord
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE LandlordID = {0} AND WorkflowStatus LIKE '%{1}%'", UserID, searchTerm)
                    Case 3 'Service Staff
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE ServiceStaffID = {0} AND WorkflowStatus LIKE '%{1}%'", UserID, searchTerm)
                End Select
                GlobalVariables.ds = GetData(queryString)
                If (GlobalVariables.ds.Tables.Count > 0) Then
                    GridView1.DataSource = GlobalVariables.ds
                    GridView1.DataBind()
                End If
            Case "service"
                Select Case RoleID
                    Case 1 'Tenant
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE TenantID = {0} AND ServiceArea LIKE '%{1}%'", UserID, searchTerm)
                    Case 2 'Landlord
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE LandlordID = {0} AND ServiceArea LIKE '%{1}%'", UserID, searchTerm)
                    Case 3 'Service Staff
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE ServiceStaffID = {0} AND ServiceArea LIKE '%{1}%'", UserID, searchTerm)
                End Select
                GlobalVariables.ds = GetData(queryString)
                If (GlobalVariables.ds.Tables.Count > 0) Then
                    GridView1.DataSource = GlobalVariables.ds
                    GridView1.DataBind()
                End If
            Case "servicestatus"
                Dim strIndWords() As String = Split(searchTerm, " ")
                Dim strSearchMultipleCriteria As String = String.Empty
                For i = 0 To strIndWords.Length() - 1
                    If i = strIndWords.Length() - 1 Then
                        strSearchMultipleCriteria &= "'" & strIndWords(i) & "'"
                    Else
                        strSearchMultipleCriteria &= "'" & strIndWords(i) & "'" & ","
                    End If
                Next
                Select Case RoleID
                    Case 1 'Tenant
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE TenantID = {0} AND ServiceArea IN ({1}) AND WorkflowStatus IN ({2})", UserID, strSearchMultipleCriteria, strSearchMultipleCriteria)
                    Case 2 'Landlord
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE LandlordID = {0} AND ServiceArea IN ({1}) AND WorkflowStatus IN ({2})", UserID, strSearchMultipleCriteria, strSearchMultipleCriteria)
                    Case 3 'Service Staff
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE ServiceStaffID = {0} AND ServiceArea IN ({1}) AND WorkflowStatus IN ({2})", UserID, strSearchMultipleCriteria, strSearchMultipleCriteria)
                End Select
                GlobalVariables.ds = GetData(queryString)
                If (GlobalVariables.ds.Tables.Count > 0) Then
                    GridView1.DataSource = GlobalVariables.ds
                    GridView1.DataBind()
                End If
            Case Else
                Select Case RoleID
                    Case 1 'Tenant
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE TenantID = {0}", UserID)
                    Case 2 'Landlord
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE LandlordID = {0}", UserID)
                    Case 3 'Service Staff
                        queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE ServiceStaffID = {0}", UserID)
                End Select
                GlobalVariables.ds = GetData(queryString)
                If (GlobalVariables.ds.Tables.Count > 0) Then
                    GridView1.DataSource = GlobalVariables.ds
                    GridView1.DataBind()
                End If
        End Select
    End Sub

    Private Sub Dashboard_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim queryString As String = String.Empty
        Dim RoleID As Integer
        Dim UserID As Integer
        Dim UserInfo As Hashtable = CType(Session("UserInfo"), Hashtable)
        With UserInfo
            RoleID = CType(.Item("RoleID"), Integer)
            UserID = CType(.Item("UserID"), Integer)
        End With
        Select Case RoleID
            Case 1 'Tenant
                queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE TenantID = {0}", UserID)
            Case 2 'Landlord
                queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE LandlordID = {0}", UserID)
            Case 3 'Service Staff
                queryString = String.Format("SELECT [ServiceRequestID], [PropertyName], [TenantFirstName], [TenantLastName], [LandlordFirstName], [LandlordLastName], [ServiceStaffFirstName], [ServiceStaffLastName], [WorkflowStatus], [ServiceArea], [SRCreateDate], [SRLastEditDate] FROM [ServiceRequestView] WHERE ServiceStaffID = {0}", UserID)
        End Select
        Dim ds As DataSet = GetData(queryString)
        If (ds.Tables.Count > 0) Then
            GridView1.DataSource = ds
            GridView1.DataBind()
        End If
    End Sub

    Private Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        If selSearchType.Value IsNot "all" Then
            GridView1.DataSource = GlobalVariables.ds
        End If
        GridView1.DataBind()
    End Sub

    Function GetData(ByVal queryString As String) As DataSet
        ' Retrieve the connection string stored in the Web.config file.
        Dim connectionString As String = ConfigurationManager.ConnectionStrings("Team116DBConnectionString").ConnectionString
        Dim ds As New DataSet()
        ' Connect to the database and run the query.
        Dim connection As New SqlConnection(connectionString)
        Dim adapter As New SqlDataAdapter(queryString, connection)
        ' Fill the DataSet.
        adapter.Fill(ds)
        Return ds
    End Function

End Class
