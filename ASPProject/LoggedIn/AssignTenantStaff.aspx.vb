﻿Option Strict On
Imports PropertyMaintDataSet
Imports PropertyMaintDataSetTableAdapters


Partial Class AssignTenantStaff
    Inherits System.Web.UI.Page

    Private Sub AssignTenantStaff_LoadComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        Dim strCurrentTenantID As String
        Dim strCurrentStaffID As String

        Using aAdapter As New PropertyTableAdapter
            Dim aTable As New PropertyDataTable
            aTable = aAdapter.GetSinglePropertyByPropertyID(Convert.ToInt32(Request.QueryString("PropID")))
            If aTable.Rows.Count = 1 Then
                Label2.Text = aTable.Rows(0).Item("Name").ToString
                strCurrentStaffID = aTable.Rows(0).Item("ServiceStaffID").ToString
                strCurrentTenantID = aTable.Rows(0).Item("TenantID").ToString
                DropDownList1.Items.FindByValue(strCurrentTenantID).Selected = True
                DropDownList2.Items.FindByValue(strCurrentStaffID).Selected = True

            Else
                Response.Redirect("~/Home.aspx")
            End If

        End Using
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim intPropertyID As Integer
        Dim intTenantID As Integer
        Dim intStaffID As Integer

        intTenantID = Convert.ToInt32(DropDownList1.SelectedValue)
        intStaffID = Convert.ToInt32(DropDownList2.SelectedValue)
        intPropertyID = Convert.ToInt32(Request.QueryString("PropID"))

        Using aAdapter As New PropertyTableAdapter
            'Hacky, but can't have a tenant in multiple properties. Clear this tenant from any other property 
            'they may be in. UserID #19 is our "Vacant" tenant.
            aAdapter.UpdateVacantTenant(intTenantID, intTenantID)
            aAdapter.UpdateTenantStaff(intTenantID, intStaffID, intPropertyID)
            Response.Redirect(String.Format("~/LoggedIn/EditProperty.aspx?PropID={0}", intPropertyID))

        End Using
    End Sub

    Private Sub AssignTenantStaff_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Session("UserRole") Is Nothing Then
            Response.Redirect("~/Home.aspx")
        End If

        If Convert.ToInt32(Session("UserRole")) <> 2 Then
            Response.Redirect("~/LoggedIn/dashboard.aspx")
        End If

        If Session("UserID") Is Nothing Then
            Response.Redirect("~/Home.aspx")
        End If
    End Sub
End Class
