
window.onload = function() {
    loadPropertyData();
}

// Populate / update service request form with existing data on select property change
function loadPropertyData() {
    
    var queryparam = getParameterByName("ServiceRequestID")
    if (queryparam > 1)
    {
        // If we are editing a existing request, we don't need all this
        return;
    }

    var properties = document.getElementById('MasterContentPlaceHolder1_ChildContentPlaceHolder1_ddPropertyList');
    var selectedProperty = properties.options[properties.selectedIndex];
    var tenantID = selectedProperty.getAttribute('data-tenantid');
    var tenantName = selectedProperty.getAttribute('data-tenantName');
    var landlordID = selectedProperty.getAttribute('data-landlordid');
    var landlordName = selectedProperty.getAttribute('data-landlordName');
    var servicestaffID = selectedProperty.getAttribute('data-servicestaffid');
    var servicestaffName = selectedProperty.getAttribute('data-staffName');
    
    
    document.getElementById('MasterContentPlaceHolder1_ChildContentPlaceHolder1_txtTenantName').value = tenantName;
    document.getElementById('MasterContentPlaceHolder1_ChildContentPlaceHolder1_txtLandlordName').value = landlordName;
    document.getElementById('MasterContentPlaceHolder1_ChildContentPlaceHolder1_txtServiceStaffName').value = servicestaffName;
    document.getElementById('MasterContentPlaceHolder1_ChildContentPlaceHolder1_HiddenTenantID').value = tenantID;
    document.getElementById('MasterContentPlaceHolder1_ChildContentPlaceHolder1_HiddenLandlordID').value = landlordID;
    document.getElementById('MasterContentPlaceHolder1_ChildContentPlaceHolder1_HiddenServiceStaffID').value = servicestaffID;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
