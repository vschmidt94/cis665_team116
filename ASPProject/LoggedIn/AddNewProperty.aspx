﻿<%@ Page Title="" Language="VB" MasterPageFile="~/PropertyMaintChildMaster.master" AutoEventWireup="false" CodeFile="AddNewProperty.aspx.vb" Inherits="AddNewProperty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ChildContentPlaceHolder1" runat="Server">
    <div class="container">
        <div class="starter-template">
            <div class="container">
                <br />
                <br />
                <br />
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="panel-title">Add New Property</div>
                        </div>
                        <div class="panel-body">
                            
                                
                                <div class="form-group">
                                    <label for="tbPropertyName" class="col-md-3 control-label">Property Name</label>
                                    <div class="col-md-9">
                                        <div class="help-block with-errors">
                                            <asp:TextBox ID="tbPropertyName" runat="server" CssClass="form-control" pattern="^[a-zA-Z0-9][\w\s\&,]*[a-zA-Z0-9\!\?\.]$" MaxLength="50" data-minlength="3" title="Property Name" data-error="Property Name has invalid characters (min 3, max 50 length)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="tbStreetAddr" class="col-md-3 control-label">Street Address</label>
                                    <div class="col-md-9">
                                        <div class="help-block with-errors">
                                            <asp:TextBox ID="tbStreetAddr" runat="server" CssClass="form-control" pattern="^[a-zA-Z0-9][\w\s\&,]*[a-zA-Z0-9\!\?\.]$" MaxLength="50" data-minlength="3" placeholder="Street Address" title="Street has invalid characters (min 3, max 50 length)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="tbCity" class="col-md-3 control-label">City</label>
                                    <div class="col-md-9">
                                        <div class="help-block with-errors">
                                            <asp:TextBox ID="tbCity" runat="server" CssClass="form-control" pattern="^[a-zA-Z0-9][\w\s\&,]*[a-zA-Z0-9\!\?\.]$" MaxLength="50" data-minlength="3" placeholder="City" title="City has invalid characters (min 3, max 50 length)"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="tbState" class="col-md-3 control-label">State</label>
                                    <div class="col-md-9">
                                        <div class="help-block with-errors">
                                            <asp:TextBox ID="tbState" runat="server" CssClass="form-control" pattern="^[A-Z]{2}" MaxLength="2" data-minlength="2" placeholder="State" title="2 Letter State abbreviation"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="tbZip" class="col-md-3 control-label">Zip Code</label>
                                    <div class="col-md-9">
                                        <div class="help-block with-errors">
                                            <asp:TextBox ID="tbZip" runat="server" CssClass="form-control" pattern="^\d{5}" MaxLength="5" data-minlength="5" placeholder="Zip Code" title="5 digit zip code"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
   
                                <div class="form-group">
                                    <label for="tbLandlord" class="col-md-3 control-label">Landlord</label>
                                    <div class="col-md-9">
                                        <div class="help-block with-errors">
                                            <asp:TextBox ID="tbLandlord" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>                            

                                <div class="form-group">
                                    
                                    <!-- Button -->
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="Button1" runat="server" Text="Add New Property" CssClass="btn btn-success" />
                                        <i class="icon-hand-right"></i>
                                       
                                    </div>
                                </div>
                           
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
</asp:Content>

