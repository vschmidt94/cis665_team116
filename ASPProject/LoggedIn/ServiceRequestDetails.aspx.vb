﻿Option Strict On
Imports ServiceRequestDataSet
Imports ServiceRequestDataSetTableAdapters


Partial Class ServiceRequestDetails
    Inherits System.Web.UI.Page

    Private Sub ServiceRequestDetails_Load(sender As Object, e As EventArgs) Handles Me.Load


        If Request.QueryString.HasKeys AndAlso Request.QueryString("ServiceRequestID") IsNot Nothing Then
            'We have been sent to view / edit an existing service request

            Dim ServiceRequestID As Integer = Convert.ToInt32(Request.QueryString("ServiceRequestID"))
            Dim aTable = New ServiceRequest1DataTable
            Using aAdapter As New ServiceRequest1TableAdapter
                aTable = aAdapter.GetServiceRequest_v_byID(ServiceRequestID)
            End Using

            'We should always get 1 row back in the table
            If aTable.Rows.Count <> 1 Then
                Response.Clear()
                Response.Redirect("~/UnknownError.aspx")
            End If



            Dim PropertyName As String = aTable.Rows(0).Item("PropertyName").ToString
            Dim TenantName As String = aTable.Rows(0).Item("TenantName").ToString
            Dim LandlordName As String = aTable.Rows(0).Item("LandlordName").ToString
            Dim ServiceStaffName As String = aTable.Rows(0).Item("StaffName").ToString
            Dim CreateDate As String = aTable.Rows(0).Item("CreateDate").ToString
            Dim UpdateDate As String = aTable.Rows(0).Item("LastEditDate").ToString

            txtServiceRequestID.Text = ServiceRequestID.ToString
            txtTenantName.ReadOnly = True
            txtTenantName.Text = TenantName
            txtLandlordName.Text = LandlordName
            txtServiceStaffName.Text = ServiceStaffName
            txtCreateDate.Text = CreateDate
            txtUpdateDate.Text = UpdateDate

            'Populate the Property Name drop down
            'ddPropertyList.Items.Clear()
            ddPropertyList.Items.Add(PropertyName)
            ddPropertyList.Items.FindByText(PropertyName).Selected = True
            ddPropertyList.Enabled = False
            ddPropertyList.CssClass = "col-md-9 form-control"


        Else
            'If here, we are going to be creating a new service request record
            Dim role As Integer = CInt(Session("UserRole"))
            Dim userID As Integer = CInt(Session("UserID"))
            Dim aTable As New Property_vDataTable
            aTable.Clear()
            If role = 2 Then
                'Landlord property list
                Using aAdapter As New Property_vTableAdapter
                    aTable = aAdapter.GetPropertiesForLandlordID(userID)
                End Using
            ElseIf role = 1 Then
                'Tenant property list
                Using aAdapter As New Property_vTableAdapter
                    aTable = aAdapter.GetPropertyByTenantID(userID)
                End Using
            ElseIf role = 3 Then
                'Service staff property list
                Using aAdapter As New Property_vTableAdapter
                    aTable = aAdapter.GetPropertiesForServiceStaffID(userID)
                End Using
            Else
                'Unexpected role dump back to dashboard as we don't know what to do
                Response.Redirect("~/LoggedIn/dashboard.aspx")
            End If

            'Loop through the table returned and use that to populate the dropdown
            For Each row As Property_vRow In aTable
                Dim strPropName As String = row("Name").ToString
                Dim propID As Integer = CInt(row("PropertyID").ToString)
                Dim tenantName As String = row("tenant_name").ToString
                Dim landlordName As String = row("landlord_name").ToString
                Dim staffName As String = row("staff_name").ToString
                Dim tenantID As String = row("TenantID").ToString
                Dim landlordID As String = row("LandlordID").ToString
                Dim staffID As String = row("ServiceStaffID").ToString
                ddPropertyList.Items.Add(New ListItem(strPropName, propID.ToString))
                ddPropertyList.Items.FindByValue(propID.ToString).Attributes.Add("data-tenantName", tenantName)
                ddPropertyList.Items.FindByValue(propID.ToString).Attributes.Add("data-landlordName", landlordName)
                ddPropertyList.Items.FindByValue(propID.ToString).Attributes.Add("data-staffName", staffName)
                ddPropertyList.Items.FindByValue(propID.ToString).Attributes.Add("data-tenantID", tenantID)
                ddPropertyList.Items.FindByValue(propID.ToString).Attributes.Add("data-landlordID", landlordID)
                ddPropertyList.Items.FindByValue(propID.ToString).Attributes.Add("data-servicestaffID", staffID)
            Next row
        End If



    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect("./Dashboard.aspx")
    End Sub

    Private Sub btnSaveComment_Click(sender As Object, e As EventArgs) Handles btnSaveComment.Click
        Dim intCommentPK As Integer
        If Session("UserInfo") IsNot Nothing Then
            Dim UserID As Integer
            Dim UserInfo As Hashtable = CType(Session("UserInfo"), Hashtable)
            With UserInfo
                UserID = CType(.Item("UserID"), Integer)
            End With
            Dim ServiceRequestID As Integer = CType(txtServiceRequestID.Text, Integer)
            Dim Comments As String = txtComment.Text
            Using aAdapter As New CommentsTableAdapter
                intCommentPK = aAdapter.InsertComment(ServiceRequestID, Comments, UserID)
                GridView1.DataBind()
            End Using
        End If
    End Sub

    Private Sub ServiceRequestDetails_PreLoad(sender As Object, e As EventArgs) Handles Me.PreLoad
        If Session("UserRole") Is Nothing Then
            Response.Redirect("./Home.aspx")
        End If

        If Session("UserID") Is Nothing Then
            Response.Redirect("./Home.aspx")
        End If

    End Sub

    Private Sub ServiceRequestDetails_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        'Are we editing existing service request or adding new one?
        If Request.QueryString.HasKeys AndAlso Request.QueryString("ServiceRequestID") IsNot Nothing Then
            'Ediiting existing request

            'Inefficient, but we need a couple of things.
            Dim ServiceRequestID As UInt32
            ServiceRequestID = Convert.ToUInt32(Request.QueryString("ServiceRequestID").ToString)

            Dim aTable As New ServiceRequest1DataTable
            Using aAdapter As New ServiceRequest1TableAdapter
                aTable = aAdapter.GetServiceRequest_v_byID(CInt(ServiceRequestID))
            End Using

            'We expect to have 1 row here, if we don't dump back to dashboard. Something is wrong
            If aTable.Rows.Count <> 1 Then
                Response.Redirect("~/dashboard.aspx")
            End If

            Dim WorkflowStatus As Integer = CInt(aTable.Rows(0).Item("WorkflowStatusID").ToString)
            Dim ServiceArea As Integer = CInt(aTable.Rows(0).Item("ServiceAreaID").ToString)


            ddWorkflowStatus.Items.FindByValue(WorkflowStatus.ToString).Selected = True
            ddServiceArea.Items.FindByValue(ServiceArea.ToString).Selected = True

            'Tenant not allowed to change status
            If Convert.ToUInt32(Session("UserRole")) = 1 Then
                ddWorkflowStatus.Enabled = False
            End If

            'Service Staff not allowed to delete
            If Convert.ToUInt32(Session("UserRole")) = 3 Then
                btnDeleteSR.Enabled = False
                btnDeleteSR.Visible = False
            End If

            'Editing existing should not show Add New Button
            btnAddNewSR.Enabled = False
            btnAddNewSR.Visible = False

            'Don't show initial comment div
            divInitialComment.Visible = False

        Else
            'Adding new request - remove the update and delete buttons
            btnDeleteSR.Enabled = False
            btnDeleteSR.Visible = False
            btnUpdateSR.Enabled = False
            btnUpdateSR.Visible = False
            'Enable the Add New Service Request Button
            btnAddNewSR.Enabled = True
            btnAddNewSR.Visible = True
            'Remove the Comment List div
            divCommentList.Visible = False
            'Replace the text in the Permissions section to be more appropriate
            divPermissionsTxt.InnerHtml = "<br /><br />Please enter details for new service request."
            ' Add some text that makes sense for new request
            txtServiceRequestID.Text = "New Service Request"
            txtCreateDate.Text = "(New)"
            txtUpdateDate.Text = "(New)"

            'Tenants can not change status. Set them to pending always.
            If Convert.ToUInt32(Session("UserRole")) = 1 Then
                ddWorkflowStatus.Enabled = False
                ddWorkflowStatus.Items.FindByText("Pending").Selected = True
                ddPropertyList.Enabled = False
            End If
        End If

    End Sub

    Protected Sub btnUpdateSR_Click(sender As Object, e As EventArgs) Handles btnUpdateSR.Click
        'We will just update the service area and workflow status no matter what.  Workflow status will only 
        'be changeable by landlord users, so we are already protected there.
        Dim ServiceRequestID As UInt32
        Dim ServiceAreaID As UInt32
        Dim WorkflowStatusID As UInt32

        ServiceRequestID = Convert.ToUInt32(Request.QueryString("ServiceRequestID").ToString)
        ServiceAreaID = Convert.ToUInt32(ddServiceArea.SelectedValue)
        WorkflowStatusID = Convert.ToUInt32(ddWorkflowStatus.SelectedValue)

        Using aAdapter As New ServiceRequestTableAdapter
            aAdapter.UpdateServiceAreaForServiceRequest(CInt(ServiceAreaID), CInt(ServiceRequestID))
            aAdapter.UpdateWorkflowStatusForServiceRequest(CInt(WorkflowStatusID), CInt(ServiceRequestID))
        End Using

        'Dump 'em back to dashboard
        Response.Redirect("~/LoggedIn/dashboard.aspx")

    End Sub
    Protected Sub btnDeleteSR_Click(sender As Object, e As EventArgs) Handles btnDeleteSR.Click
        Dim ServiceRequestID As UInt32
        ServiceRequestID = Convert.ToUInt32(Request.QueryString("ServiceRequestID").ToString)
        'Delete all the comments for the service request first
        Using aAdapter As New CommentsTableAdapter
            aAdapter.DeleteCommentsByRequestID(CInt(ServiceRequestID))
        End Using
        'Now we can delete the original service request
        Using aAdapter As New ServiceRequestTableAdapter
            aAdapter.DeleteServiceRequestByID(CInt(ServiceRequestID))
        End Using

        'Dump them back to the dashboard
        Response.Redirect("~/LoggedIn/dashboard.aspx")
    End Sub

    Protected Sub btnAddNewSR_Click(sender As Object, e As EventArgs) Handles btnAddNewSR.Click
        Dim propID As Integer
        Dim tenantID As Integer
        Dim landlordID As Integer
        Dim staffID As Integer
        Dim requestID As Integer
        Dim comment As String


        propID = CInt(ddPropertyList.SelectedValue)
        tenantID = CInt(HiddenTenantID.Value)
        landlordID = CInt(HiddenLandlordID.Value)
        staffID = CInt(HiddenServiceStaffID.Value)

        comment = txtInitialComment.Text


        Using aAdapter As New ServiceRequestTableAdapter
            aAdapter.InsertNewServiceRequest(propID, landlordID, tenantID, staffID, CInt(ddWorkflowStatus.SelectedValue), CInt(ddServiceArea.SelectedValue))
            requestID = CInt(aAdapter.GetLastInsertedServiceRequestID())

        End Using

        Using aAdapter As New CommentsTableAdapter
            aAdapter.InsertComment(requestID, comment, CInt(Session("UserID")))
        End Using

        Response.Redirect("~/LoggedIn/dashboard.aspx")
    End Sub
End Class
