﻿Option Strict On
Imports PropertyMaintDataSet
Imports PropertyMaintDataSetTableAdapters
Imports ServiceRequestDataSet
Imports ServiceRequestDataSetTableAdapters



Partial Class EditProperty
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect(String.Format("~/LoggedIn/AssignTenantStaff.aspx?PropID={0}", Request.QueryString("PropID").ToString))
    End Sub
    Protected Sub DetailsView1_PageIndexChanging(sender As Object, e As DetailsViewPageEventArgs) Handles DetailsView1.PageIndexChanging

    End Sub

    Private Sub EditProperty_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Request.QueryString("PropID") Is Nothing Then
            Response.Redirect("~/LoggedIn/dashboard.aspx")
        End If
        ' Need to make sure we really can get a record. If the user hit DELETE in details view, it may try to
        ' return here, but nothing to see at that point.
        Using aAdapter As New PropertyTableAdapter
            Dim aTable As New PropertyDataTable
            aTable = aAdapter.GetSinglePropertyByPropertyID(CInt(Request.QueryString("PropID").ToString))
            If aTable.Rows.Count <> 1 Then
                Response.Clear()
                Response.Redirect("~/LoggedIn/dashboard.aspx")
            End If

        End Using
    End Sub

    Private Sub ObjectDataSource1_Deleted(sender As Object, e As ObjectDataSourceStatusEventArgs) Handles ObjectDataSource1.Deleted
        Response.Redirect("~/LoggedIn/dashboard.aspx")
    End Sub

    Private Sub ObjectDataSource1_Deleting(sender As Object, e As ObjectDataSourceMethodEventArgs) Handles ObjectDataSource1.Deleting
        Dim PropertyDeletedID As String
        PropertyDeletedID = e.InputParameters.Item(0).ToString
        ' Get all the service requests for this property
        Dim aAdapter = New ServiceRequestTableAdapter
        Using aAdapter
            Dim aTable = New ServiceRequestDataTable
            aTable = aAdapter.GetAllServiceRequestsForPropertyID(CInt(PropertyDeletedID))
            For Each aRow As ServiceRequestRow In aTable
                ' Delete all the comments for this service request
                Using commentTableAdapter As New CommentsTableAdapter
                    commentTableAdapter.DeleteAllCommentsForRequestID(CInt(aRow.ServiceRequestID))
                End Using
                ' Delete this service request
                aAdapter.DeleteServiceRequestByID(CInt(aRow.ServiceRequestID))

            Next
        End Using

    End Sub
End Class
