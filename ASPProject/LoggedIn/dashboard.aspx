﻿<%@ Page Title="" Language="VB" MasterPageFile="~/PropertyMaintChildMaster.master" AutoEventWireup="false" CodeFile="Dashboard.aspx.vb" Inherits="Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ChildContentPlaceHolder1" runat="Server">
    <br />
    <br />
    <br />
    <div class="form-group">
        <br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Dashboard View"></asp:Label>
        <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered" AutoGenerateColumns="True" AllowPaging="True" AutoGenerateSelectButton="True" PageSize="20" PagerSettings-Mode="NextPreviousFirstLast" PagerSettings-NextPageText="Next&nbsp;" PagerSettings-PreviousPageText="Previous&nbsp;" PagerSettings-FirstPageText="First&nbsp;" PagerSettings-LastPageText="Last&nbsp;">
        </asp:GridView>
    </div>
    <br />
    <br />
    <div class="container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
                    <div class="input-group">
                        <div class="input-group-btn search-panel">
                            <select id="selSearchType" name="search-type" runat="server" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                <option value="all">All</option>
                                <option value="status">Status</option>
                                <option value="service">Service Area</option>
                                <option value="servicestatus">Service Area & Status</option>
                            </select>
                        </div>
                        <asp:TextBox ID="txtSearchTerm" class="form-control" placeholder="Search" runat="server"></asp:TextBox>
                        <span class="input-group-btn">
                            <button id="btnSearch" class="btn btn-success" runat="server"><span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

