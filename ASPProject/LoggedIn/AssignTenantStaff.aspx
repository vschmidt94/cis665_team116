﻿<%@ Page Title="Assign Tenant & Staff" Language="VB" MasterPageFile="~/PropertyMaintChildMaster.master" AutoEventWireup="false" CodeFile="AssignTenantStaff.aspx.vb" Inherits="AssignTenantStaff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ChildContentPlaceHolder1" runat="Server">
    <div class="container" dir="auto">
        <br />
        <asp:Label ID="Label1" runat="server" Text="Assign Tenant & Staff to Property" Font-Size="XX-Large"></asp:Label>
        <br />
        <br />
        <div class="panel panel-success">
            <div class="panel panel-heading">For Property:</div>
            <div class="panel panel-body"><asp:Label ID="Label2" runat="server" Text="Label" Font-Size="Large"></asp:Label></div>
        </div>
        <asp:Label ID="Label3" runat="server" Text="Tenant:" Font-Size="Large"></asp:Label>
&nbsp;<br />
        <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource1" DataTextField="tName" DataValueField="UserID" Font-Size="Large" CssClass="form-control">
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByTenants" TypeName="PropertyMaintDataSetTableAdapters.UserTableAdapter"></asp:ObjectDataSource>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="Service Staff:" Font-Size="Large"></asp:Label>
        <br />
        <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="ObjectDataSource2" DataTextField="sName" DataValueField="UserID" Font-Size="Large" CssClass="form-control">
        </asp:DropDownList>
        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByStaff" TypeName="PropertyMaintDataSetTableAdapters.UserTableAdapter"></asp:ObjectDataSource>
        <br />
        <br />
          <asp:Button ID="Button1" runat="server" Text="Update " class="btn btn-success" />
          <br />
        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="PropertyMaintDataSetTableAdapters.PropertyTableAdapter" UpdateMethod="UpdateTenantStaff">
            <UpdateParameters>
                <asp:Parameter Name="TenantID" Type="Int32" />
                <asp:Parameter Name="ServiceStaffID" Type="Int32" />
                <asp:Parameter Name="PropertyID" Type="Int32" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <br />
        
    </div>
</asp:Content>

