﻿<%@ Page Title="Edit Property" Language="VB" MasterPageFile="~/PropertyMaintChildMaster.master" AutoEventWireup="false" CodeFile="EditProperty.aspx.vb" Inherits="EditProperty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ChildContentPlaceHolder1" Runat="Server">
    <div class="container">
    <br />
    <asp:Label ID="Label1" runat="server" Text="Edit Property" Font-Size="XX-Large"></asp:Label>
    <br />
        
    Use the form below to edit or delete the property record<br />
        .
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="PropertyID" DataSourceID="ObjectDataSource1" CssClass="table table-bordered">
            <Fields>
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                <asp:BoundField DataField="ZipCode" HeaderText="ZipCode" SortExpression="ZipCode" />
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            </Fields>
        </asp:DetailsView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetSinglePropertyByPropertyID" TypeName="PropertyMaintDataSetTableAdapters.PropertyTableAdapter" UpdateMethod="UpdatePropertyAddress">
            <DeleteParameters>
                <asp:Parameter Name="Original_PropertyID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="ZipCode" Type="Int32" />
                <asp:Parameter Name="TenantID" Type="Int32" />
                <asp:Parameter Name="LandlordID" Type="Int32" />
                <asp:Parameter Name="ServiceStaffID" Type="Int32" />
                <asp:Parameter Name="CreateDate" Type="DateTime" />
            </InsertParameters>
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="-1" Name="PropertyID" QueryStringField="PropID" Type="Int32" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="Address" Type="String" />
                <asp:Parameter Name="City" Type="String" />
                <asp:Parameter Name="State" Type="String" />
                <asp:Parameter Name="ZipCode" Type="Int32" />
                <asp:Parameter Name="Original_PropertyID" Type="Int32" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    <br />
        <asp:LinkButton ID="Button1" runat="server" Text="Assign Tenants and Service Staff" CssClass="btn btn-success"/>
</asp:Content>

