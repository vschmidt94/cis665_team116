﻿Option Strict On
Imports PropertyMaintDataSet
Imports PropertyMaintDataSetTableAdapters

Partial Class PropertyMaintenance
    Inherits System.Web.UI.Page

    Private Sub PropertyMaintenance_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Session("UserRole") Is Nothing Then
            Response.Redirect("./Home.aspx")
        End If

        If Convert.ToInt32(Session("UserRole")) <> 2 Then
            Response.Redirect("./dashboard.aspx")
        End If

        If Session("UserID") Is Nothing Then
            Response.Redirect("./Home.aspx")
        End If
    End Sub
End Class
