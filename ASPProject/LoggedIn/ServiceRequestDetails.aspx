﻿<%@ Page Title="" Language="VB" MasterPageFile="~/PropertyMaintChildMaster.master" AutoEventWireup="false" CodeFile="ServiceRequestDetails.aspx.vb" Inherits="ServiceRequestDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ChildContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script src='<%=ResolveClientUrl("~/LoggedIn/javascript/servicerequest.js") %>' 
type="text/javascript"></script>
    <br />
    <br />
    <br />
    <div class="container">
        <div class="panel panel-success">
            <div class="panel-heading">Service Request Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="requestID" class="col-md-3 control-label">Request ID</label>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtServiceRequestID" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                    </div>
                </div>
                <br />
                <br />
                <div class="form-group">
                    <label for="property" class="col-md-3 control-label">Property Name:</label>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddPropertyList" runat="server" class="col-md-9 form-control" onchange="loadPropertyData()" >
                        </asp:DropDownList>
                    </div>
                </div>
                <br />
                <br />
                <div class="form-group">
                    <label for="tenant" class="col-md-3 control-label">Tenant</label>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtTenantName" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="HiddenTenantID" runat="server" />
                    </div>
                    
                    <label for="landlord" class="col-md-3 control-label">Landlord</label>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtLandlordName" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="HiddenLandlordID" runat="server" />
                    </div>
                </div>
                <br />
                <br />
                <div class="form-group">
                    <label for="serviceStaff" class="col-md-3 control-label">Service Staff Assigned</label>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtServiceStaffName" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                        <asp:HiddenField ID="HiddenServiceStaffID" runat="server" />
                    </div>
                    <label for="workflowStatus" class="col-md-3 control-label">Workflow Status</label>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddWorkflowStatus" runat="server" DataSourceID="ObjectDataSource2" DataTextField="WorkflowStatus" DataValueField="WorkflowStatusID" CssClass="form-control col-md-3">
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ServiceRequestDataSetTableAdapters.WorkflowStatusTableAdapter"></asp:ObjectDataSource>
                    </div>
                </div>
                <br />
                <br />
                <div class="form-group">
                    <label for="serviceArea" class="col-md-3 control-label">Service Area</label>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddServiceArea" runat="server" class="col-md-2 form-control" DataSourceID="ObjectDataSource3" DataTextField="ServiceArea" DataValueField="ServiceAreaID">
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ServiceRequestDataSetTableAdapters.ServiceAreaTableAdapter"></asp:ObjectDataSource>
                    </div>
                    <br />
                    <br />
                </div>
                <div id="divInitialComment" Class="form-group" runat="server">
                    <label for="initialComment" class="col-md-3 control-label">Initial Comment</label>
                    <div class="col-md-9">
                        <asp:TextBox ID="txtInitialComment" class="form-control" runat="server" placeholder="Initial Comment"></asp:TextBox>
                    </div>
                    <br />
                    <br />
                </div>
                <div class="form-group">
                    <label for="createdDate" class="col-md-3 control-label">Created Date</label>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtCreateDate" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                    </div>
                    <label for="lastEditDate" class="col-md-3 control-label">Last Updated Date</label>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtUpdateDate" class="form-control" ReadOnly="true" runat="server"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div id="UpdateButtons" class="row">
                    <div id="divPermissionsTxt" class="col-md-6" runat="server">
                        <br />
                        Permissions:<br />
                        Service Area can be updated by Tenant, Landlord, and Staff.<br />
                        Workflow Status can be updated by Landlords and Staff.<br />
                        Requests may only be deleted by Landlords or Tenants.<br />
                        All users can create a new request.<br />
                        All users can add additional comments below.<br />
                    </div>
                    <div class="col-md-6">
                        <br />
                        <br />
                        <br />
                        <asp:Button ID="btnUpdateSR" runat="server" class="btn btn-success" Text="Update Service Request" />
                        &nbsp<asp:Button ID="btnDeleteSR" runat="server" class="btn btn-danger" Text="Delete Service Request" />
                        &nbsp<asp:Button ID="btnAddNewSR" runat="server" class="btn btn-success" Text="Add New Service Request" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divCommentList" class="container" runat="server">
        <div class="panel panel-success">
            <div class="panel-heading">Comments Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <asp:GridView ID="GridView1" CssClass="table table-bordered" runat="server" DataSourceID="ObjectDataSource1" AutoGenerateColumns="False" DataKeyNames="CommentID" AllowPaging="True">
                        <Columns>
                            <asp:BoundField DataField="Comments" HeaderText="Comment" SortExpression="Comments"></asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName"></asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName" />
                            <asp:BoundField DataField="Role" HeaderText="Role" SortExpression="Role" />
                            <asp:BoundField DataField="CommentDate" HeaderText="Comment Date" SortExpression="CommentDate" />
                        </Columns>
                    </asp:GridView>
                    <asp:ObjectDataSource runat="server" ID="ObjectDataSource1" OldValuesParameterFormatString="original_{0}" SelectMethod="GetCommentsForServiceRequestID" TypeName="ServiceRequestDataSetTableAdapters.CommentsViewTableAdapter">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="-1" Name="ServiceRequestID" QueryStringField="ServiceRequestID" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-9">
                        <asp:Button ID="btnAddComment" class="btn btn-success" runat="server" Text="Add Comment" /><i class="icon-hand-right"></i>
                        <asp:Button ID="btnCancel" class="btn btn-success" runat="server" Text="Cancel" /><i class="icon-hand-right"></i>
                    </div>
                </div>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" PopupControlID="Panel1" TargetControlID="btnAddComment"
                    CancelControlID="btnCancelComment" runat="server">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server">
                    <div class="container">
                        <div class="panel panel-success">
                            <div class="panel-heading">Add Comment</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="lblComment" class="col-md-3 control-label">Comment</label>
                                        <div class="col-md-9">
                                            <asp:TextBox ID="txtComment" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <br />
                                    <br />
                                    <br />
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-9">
                                            <asp:Button ID="btnSaveComment" class="btn btn-success" runat="server" Text="Save Comment" /><i class="icon-hand-right"></i>
                                            <asp:Button ID="btnCancelComment" class="btn btn-success" runat="server" Text="Cancel" /><i class="icon-hand-right"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>

