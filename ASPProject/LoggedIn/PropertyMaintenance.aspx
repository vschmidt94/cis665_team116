﻿<%@ Page Title="" Language="VB" MasterPageFile="~/PropertyMaintChildMaster.master" AutoEventWireup="false" CodeFile="PropertyMaintenance.aspx.vb" Inherits="PropertyMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ChildContentPlaceHolder1" Runat="Server">
    <div class="container">
    <br />
    <asp:Label ID="Label1" runat="server" Text="Property Maintenance" Font-Size="XX-Large"></asp:Label>
    <br />
        <div class="panel panel-success">
            <div class="panel-heading">
                Landlord Actions
            </div>
            <div class="panel-body">
                Click on Property Name to edit details, or<br />
                <a href="AddNewProperty.aspx" class="btn btn-success" role="button">Add New Property</a>
            </div>
        </div>
    Click on Property Name to edit details.
    <br />
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellPadding="3" CellSpacing="3" DataKeyNames="PropertyID" DataSourceID="ObjectDataSource1" ShowHeaderWhenEmpty="True" CssClass="table table-bordered">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="PropertyID" DataNavigateUrlFormatString="~/LoggedIn/EditProperty.aspx?PropID={0}" DataTextField="Name" HeaderText="Property Name" />
            <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
            <asp:BoundField DataField="ZipCode" HeaderText="Zip" SortExpression="ZipCode" />
            <asp:BoundField DataField="LandlordName" HeaderText="Landlord" ReadOnly="True" SortExpression="LandlordName" />
            <asp:BoundField DataField="TenantName" HeaderText="Tenant" ReadOnly="True" SortExpression="TenantName" />
            <asp:BoundField DataField="StaffName" HeaderText="Service Staff" ReadOnly="True" SortExpression="StaffName" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetPropertiesByLandlordID" TypeName="PropertyMaintDataSetTableAdapters.PropertyUserName_vTableAdapter">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="%" Name="LandlordID" SessionField="UserID" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
        </div>
        
</asp:Content>

