﻿Option Strict On
Imports PropertyMaintDataSet
Imports PropertyMaintDataSetTableAdapters
Imports UserRoleDataSet
Imports UserRoleDataSetTableAdapters


Partial Class AddNewProperty
    Inherits System.Web.UI.Page

    Private Sub AddNewProperty_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        If Session("UserRole") Is Nothing Then
            Response.Redirect("~/Home.aspx")
        End If

        If Convert.ToInt32(Session("UserRole")) <> 2 Then
            Response.Redirect("~/LoggedIn/dashboard.aspx")
        End If

        If Session("UserID") Is Nothing Then
            Response.Redirect("~/Home.aspx")
        End If

        Using aAdapter As New UserRoleDataSetTableAdapters.UserTableAdapter

            Dim aTable As New UserRoleDataSet.UserDataTable
            Dim userID As Integer
            userID = Convert.ToInt32(Session("UserID"))
            aTable = aAdapter.GetUserByUserID(userID)
            If aTable.Rows.Count = 1 Then
                tbLandlord.Text = aTable.Rows(0).Item("LastName").ToString
                tbLandlord.Text += ", "
                tbLandlord.Text += aTable.Rows(0).Item("FirstName").ToString
            End If
            tbLandlord.Enabled = False

        End Using

    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim landlordID As Integer
        Dim tenantID As Integer
        Dim staffID As Integer

        'Bad, magic number but 19 is our "Vacant tenant id" and 2023 is "unassigned service staff"
        tenantID = 19
        staffID = 2023
        landlordID = Convert.ToInt32(Session("UserID"))

        Using aAdapter As New PropertyTableAdapter
            aAdapter.InsertNewProp(tbPropertyName.Text, tbStreetAddr.Text, tbCity.Text, tbState.Text, Convert.ToInt32(tbZip.Text), tenantID, landlordID, staffID)
        End Using

        Response.Redirect("~/LoggedIn/PropertyMaintenance.aspx")

    End Sub
End Class
