﻿Option Strict On
Imports UserRoleDataSet
Imports UserRoleDataSetTableAdapters

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim intUserPK As Integer
        Using aAdapter As New UserTableAdapter
            Dim aTable As UserDataTable
            aTable = aAdapter.GetUserByUserName(username.Text)
            If aTable.Rows.Count = 1 Then
                Label1.Text = "Please choose a different Username"
                firstname.Text = aTable.Rows(0).Item("FirstName").ToString
                lastname.Text = aTable.Rows(0).Item("LastName").ToString
                username.Text = aTable.Rows(0).Item("UserName").ToString
                password.Text = aTable.Rows(0).Item("Password").ToString
                email.Text = aTable.Rows(0).Item("Email").ToString
                role.SelectedValue = aTable.Rows(0).Item("RoleID").ToString
            Else
                intUserPK = aAdapter.InsertNewUser(firstname.Text, lastname.Text, username.Text, password.Text, email.Text, Convert.ToInt32(role.SelectedValue), "Active")
                Response.Redirect("./Home.aspx")
            End If
        End Using
    End Sub
End Class
