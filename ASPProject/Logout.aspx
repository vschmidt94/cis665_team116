﻿<%@ Page Title="Logout" Language="VB" MasterPageFile="~/PropertyMaintChildMaster.master" AutoEventWireup="false" CodeFile="Logout.aspx.vb" Inherits="Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ChildContentPlaceHolder1" runat="Server">
    <div class="container">
        <br /><br /><br />
        <div class="panel panel-success">
            <div class="panel-heading">Goodbye!</div>
            <div class="panel-body">You have been logged out of the Property Maintanance Portal.<br />
                <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" Text="Return to Login Page" />
            </div>
        </div>
    </div>
</asp:Content>

