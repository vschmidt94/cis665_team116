﻿Option Strict On
Partial Class PropertyMaintChildMaster
    Inherits System.Web.UI.MasterPage

    Private Sub NavigationMenu_Init(sender As Object, e As EventArgs) Handles NavigationMenu.Init
        Dim RoleID As Integer
        Dim UserInfo As Hashtable = CType(Session("UserInfo"), Hashtable)
        If UserInfo IsNot Nothing Then
            With UserInfo
                RoleID = CType(.Item("RoleID"), Integer)
            End With
            If RoleID = 1 Or RoleID = 3 Then
                NavigationMenu.Items(2).Enabled = False
                NavigationMenu.Items(2).Text = "<span class=" + "text-muted" + ">Property Maintenance</span>"
                If (NavigationMenu.Items(2).Text.Contains("Property")) Then
                    NavigationMenu.Items.RemoveAt(2)
                End If
            End If
        End If
    End Sub
End Class

