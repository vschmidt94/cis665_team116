﻿Option Strict On
Imports UserRoleDataSet
Imports UserRoleDataSetTableAdapters
Partial Class Home
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim UserInfo As New Hashtable
        Using aAdapter As New UserTableAdapter
            Dim aTable As UserDataTable
            aTable = aAdapter.GetUserByUserNameAndPassword(username.Text, password.Text)
            If aTable.Rows.Count = 1 Then
                With UserInfo
                    .Add("UserName", aTable.Rows(0).Item("UserName").ToString)
                    .Add("RoleID", aTable.Rows(0).Item("RoleID").ToString)
                    .Add("UserID", aTable.Rows(0).Item("UserID").ToString)
                End With
                Session.Add("UserInfo", UserInfo)
                'Having a hard time access hash table entries in DataSource Config GUI, trying to add userID as top-level entry and see if that is OK (VS)
                Session.Add("UserID", aTable.Rows(0).Item("UserID").ToString)
                Session.Add("UserRole", aTable.Rows(0).Item("RoleID").ToString)
                If Request.QueryString("ReturnURL") <> String.Empty Then
                    ' Set authentication cookie and redirect the user to the page he/she was originally trying to get to
                    FormsAuthentication.RedirectFromLoginPage(aTable.Rows(0).Item("UserName").ToString, False)
                Else
                    'Otherwise, set authentication cookie and redirect to dashboard page
                    FormsAuthentication.SetAuthCookie(aTable.Rows(0).Item("UserName").ToString, False)
                    Response.Redirect("~/LoggedIn/dashboard.aspx")
                End If
            Else
                Label1.Text = "Invalid credentials or user does not exist."
            End If
        End Using
    End Sub
End Class
