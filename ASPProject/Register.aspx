﻿<%@ Page Title="" Language="VB" MasterPageFile="~/PropertyMaintMaster.master" AutoEventWireup="false" CodeFile="register.aspx.vb" Inherits="_Default" Theme="Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MasterContentPlaceHolder1" Runat="Server">
        <div class="container">
        <div class="starter-template">
            <div class="container">
                <div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <div class="panel-title">Sign Up</div>
                            <div style="float:right; font-size: 85%; position: relative; top:-10px">
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="./Home.aspx">Sign In</asp:HyperLink>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form runat="server" id="signupform" class="form-horizontal">

                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <asp:textbox type="email" class="form-control" ID="email" name="email" placeholder="Email Address" maxlength="75" autofocus="true" data-error="email address is invalid" required="true" runat="server"></asp:textbox>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="firstname" class="col-md-3 control-label">First Name</label>
                                    <div class="col-md-9">
                                        <asp:textbox pattern="^[a-zA-Z0-9][\w\s\&,]*[a-zA-Z0-9\!\?\.]$" class="form-control" ID="firstname" name="firstname" placeholder="First Name" maxlength="50" data-error="First name has invalid characters" required="true" runat="server"></asp:textbox>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastname" class="col-md-3 control-label">Last Name</label>
                                    <div class="col-md-9">
                                        <asp:textbox pattern="^[a-zA-Z0-9][\w\s\&,]*[a-zA-Z0-9\!\?\.]$" class="form-control" ID="lastname" name="lastname" placeholder="Last Name" maxlength="50" data-error="Last name has invalid characters" required="true" runat="server"></asp:textbox>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="username" class="col-md-3 control-label">Username</label>
                                    <div class="col-md-9">
                                        <asp:textbox data-minlength="8" class="form-control" ID="username" name="username" maxlength="50" placeholder="Username" required="true" runat="server"></asp:textbox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <asp:textbox type="password" data-minlength="8" class="form-control" ID="password" name="password" maxlength="50" placeholder="Password" required="true" runat="server"></asp:textbox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role" class="col-md-3 control-label">Role</label>
                                    <div class="col-md-9">
                                        <asp:DropDownList ID="role" class="form-control" runat="server" DataSourceID="ObjectDataSource1" DataTextField="Role" DataValueField="RoleID"></asp:DropDownList>
                                        <asp:ObjectDataSource runat="server" ID="ObjectDataSource1" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="UserRoleDataSetTableAdapters.RoleTableAdapter"></asp:ObjectDataSource>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <!-- Button -->
                                    <div class="col-md-offset-3 col-md-9">
                                        <asp:Button ID="Button1" runat="server" name="register" class="btn btn-success" Text=" Sign Up" /><i class="icon-hand-right"></i>
                                        <span class="help-inline"><strong><asp:Label ID="Label1" runat="server" Text=""></asp:Label></strong></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</asp:Content>

