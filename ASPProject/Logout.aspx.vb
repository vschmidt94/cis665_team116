﻿Option Strict On
Partial Class Logout
    Inherits System.Web.UI.Page

    Private Sub Logout_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Session.Clear()
        Session.Remove("UserInfo")
        Session.Remove("UserID")
        Session.Remove("UserRole")
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("./home.aspx")
    End Sub
End Class
